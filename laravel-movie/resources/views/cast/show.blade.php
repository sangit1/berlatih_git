@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row py-5">
                    <div class="col-md-4">
                        <img src="{{ asset('images/noimage.jpg') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <h5>{{ $cast->nama }}</h5>
                        <p>{{ $cast->umur }}</p>
                        <p><small>{{ $cast->bio }}</small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
