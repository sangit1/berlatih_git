@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success my-2 p-3" role="alert">
            {{ session('status') }}
        </div>
    @endif

    @if (session('danger'))
        <div class="alert alert-danger my-2 p-3" role="alert">
            {{ session('danger') }}
        </div>
    @endif

    <div class="mb-3">
        <a href="cast/create" type="button" class="btn btn-primary btn-sm">
            Create
        </a>
    </div>
    <div class="row">
        @foreach ($casts as $cast)
            <div class="col-md-4 mb-3">
                <div class="card testimonial-card">
                    <div class="card-up indigo lighten-1"></div>

                    <div class="avatar mx-auto white text-center mt-2">
                        <img src="{{ asset('images/noimage.jpg') }}" class="rounded-circle" alt="woman avatar" width="50%">
                    </div>

                    <div class="card-body text-center">
                        <h5 class="card-title text-muted">{{ $cast->nama }}</h5>
                        <p class="text-secondary">{{ $cast->umur }} tahun</p>
                        <p><small>{{ $cast->bio }}</small></p>
                    </div>
                    <div class="card-footer">
                        <a href="cast/{{ $cast->id }}/show" type="button" class="btn btn-info btn-sm"><i
                                class="fas fa-eye "></i> Show</a>
                        <a href="cast/{{ $cast->id }}/edit" type="button" class="btn btn-warning btn-sm"><i
                                class="fas fa-edit "></i> Edit</a>
                        <form action="/cast/{{ $cast->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
