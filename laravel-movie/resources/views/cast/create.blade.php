@extends('layouts.app')

@section('content')
    <!--Header-->
    <div class="modal-header primary-color white-text w-50">
        <h4 class="title">
            <i class="fa fa-pencil-alt"></i> Create Cast
        </h4>
        <a href="/cast" class="close waves-effect waves-light">
            <span aria-hidden="true">×</span>
        </a>
    </div>
    <!-- Card -->
    <div class="card w-50">
        <!-- Card body -->
        <div class="card-body">
            <!-- Material form register -->
            <form method="POST" action="/cast">
                @csrf
                <div class="md-form">
                    <input type="text" name="nama" class="form-control @error('nama') border border-danger @enderror"
                        placeholder="nama pemeran..." value="{{ old('nama') }}">
                </div>
                @error('nama')
                    <div class="text-danger text-sm mt-2">
                        {{ $message }}
                    </div>
                @enderror
                <div class="md-form">
                    <input type="text" name="umur" class="form-control @error('umur') border border-danger @enderror"
                        placeholder="umur pemeran..." value="{{ old('umur') }}">
                </div>
                @error('umur')
                    <div class="text-danger text-sm mt-2">
                        {{ $message }}
                    </div>
                @enderror
                <div class="md-form">
                    <textarea type="text" name="bio"
                        class="md-textarea form-control @error('bio') border border-danger @enderror"
                        placeholder="biografi pemeran...">{{ old('bio') }}</textarea>
                </div>
                @error('bio')
                    <div class="text-danger text-sm mt-2">
                        {{ $message }}
                    </div>
                @enderror

                <div class="text-center py-4 mt-3">
                    <button class="btn btn-cyan" type="submit">Create</button>
                </div>
            </form>
            <!-- Material form register -->

        </div>
        <!-- Card body -->

    </div>
    <!-- Card -->
@endsection
