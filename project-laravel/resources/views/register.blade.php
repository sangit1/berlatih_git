<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Laravel || Form</title>
</head>

<body>
    <h1>Buat Account Baru !</h1>
    <h2>Sign Up Form</h2>
    <form action="{{ url('welcome') }}" method="GET">
        @csrf
        <label for="">First Name :</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label for="">Last Name :</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label for="">Gender</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label for="">Nationality</label><br><br>
        <select name="nationality">
            <option value='1'>Indonesians</option>
            <option value='2'>Singaporeans</option>
            <option value='3'>Malaysians</option>
            <option value='4'>Australians</option>
        </select><br><br>
        <label for="">Language Spoken :</label><br><br>
        <input type="checkbox" name="bahasa_indonesia">
        <label for=""> Bahasa Indonesia</label><br>
        <input type="checkbox" name="english">
        <label for=""> English</label><br>
        <input type="checkbox" name="other">
        <label for=""> Other</label><br><br>
        <label for="">Bio:</label><br>
        <textarea rows="4" col="50"></textarea><br><br>
        <button type="submit" name="submit">Sign Up</button>
    </form>
</body>

</html>
