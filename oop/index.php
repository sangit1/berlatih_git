<?php

require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animals('Shaun');

echo "Name: " . $sheep->name . '<br>';
echo "Legs: " . $sheep->legs . '<br>';
echo "Cold Blooded : " . $sheep->cold_blooded . '<br>';

echo '<br>';
echo '<br>';

$katak = new Frog('buduk');

echo "Name: " . $katak->name . '<br>';
echo "Legs: " . $katak->legs . '<br>';
echo "Cold Blooded: " . $katak->cold_blooded . '<br>';
echo "Jump: " . $katak->jump() . '<br>';

echo '<br>';
echo '<br>';

$sungokong = new Ape('kera sakti');

echo "Name: " . $sungokong->name . '<br>';
echo "Legs: " . $sungokong->legs . '<br>';
echo "Cold Blooded: " . $sungokong->cold_blooded . '<br>';
echo "Yell: " . $sungokong->yell() . '<br>';
