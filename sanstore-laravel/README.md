# Final Project

## Kelompok 8
## Anggota Kelompok :

 - Martha Kurnia Adhi
 - Mufid Al Fikri
 - Jedidiah
## Tema Project
 Tema project Laravel kami adalah **E-commerce**
## ERD
![Entity Relationship Diagram](./public/images/site/erd.jpg)
## Link Video
 - Link demo aplikasi : https://youtu.be/dSFSeOWTo60
 - Link deployment : https://sanstore-ecommerce.herokuapp.com/



