<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\OrderController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\{Route, Auth};


Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/', 'FrontendController@index')->name('index');
Route::get('category/{id}', 'FrontendController@category');
Route::get('product', 'FrontendController@product');
Route::get('view-product/{id}', 'FrontendController@view_product');

Route::post('order/{id}', 'OrderController@index');
Route::get('checkout', 'OrderController@checkout');
Route::delete('checkout/{id}', 'OrderController@delete');
Route::get('cancel', 'OrderController@cancel');
Route::get('confirm', 'OrderController@confirm');
Route::get('final', 'OrderController@final');

Route::get('history', 'HistoryController@index');
Route::get('history/{id}', 'HistoryController@detail');

Route::get('profile', 'ProfileController@index');
Route::post('profile', 'ProfileController@create');
Route::post('profile/{id}', 'ProfileController@update');

Route::get('test', 'TestController@index');
Route::get('test/pdf', 'TestController@createPDF');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('role:admin')->group(function () {
    Route::get('/admin', 'OrderController@adminindex');
    Route::get('category', 'Admin\CategoryController@index');
    Route::get('category-create', 'Admin\CategoryController@create');

    // Route untuk olah data kategori
    Route::get('category', 'Admin\CategoryController@index');
    Route::get('category-create', 'Admin\CategoryController@create');
    Route::post('buatkategoribaru', 'Admin\CategoryController@store');
    Route::put('updatekategori/{post_id}', 'Admin\CategoryController@updatekategori');
    Route::delete('/hapuskategori/{post_id}', 'Admin\CategoryController@destroy');

   //Route untuk olah data produk
    Route::get('dataproduct', 'ProductController@index');
    Route::post('buatprodukbaru', 'ProductController@store');
    Route::put('updateproduk/{post_id}', 'ProductController@updateproduk');
    Route::delete('/hapusproduk/{post_id}', 'ProductController@destroy');

    //Route untuk olah data pemesanan ( Mengubah status pesan dari 1 menjadi 2 )
    Route::get('pesanan', 'ConfirmController@index');
    Route::put('konfirmasipembayaran/{order_id}', 'ConfirmController@konfirmasipembayaran');

});
