@extends('layouts.admin')

@section('content')

<style>

input, textarea {
	border:none;
	border-bottom:1px solid hsl(1,1%,90%);
	background:transparent;
	width:100%;
}
/* Start Style untuk tabel_kategori */
.tabel_kategori {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.tabel_kategori td, .tabel_kategori th {
  border: 1px solid #ddd;
  padding: 8px;
}


.tabel_kategori th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: hsl(220,100%,40%);
  color: white;
}

td > img {
	width:100%;
}
/* End Style untuk tabel_kategori */

/*Start Input Modal */

.inputbutton {
	font-weight: :bold;
	margin:1%;
	padding:1%;
	border-radius:5px;
	color:white;
	background:hsl(220,100%,40%);
}
.inputbutton a {
	color:white;
	font-weight:bold;
}
.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 20px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 60%;
  position: relative;
  transition: all 5s ease-in-out;
  overflow: scroll;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

/* End Input Modal*/

</style>
<div class="container_kategori" style="padding:1%">
  <a class="nav-link float-right"style="text-decoration:none;" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i> Toggle Sidebar </a><br><br>
<h1>Daftar Pesanan</h1>

<table class="tabel_kategori">
  <tr>
	<th>Username</th>
	<th>Tanggal</th>
	<th>Status</th>
	<th>Jumlah Harga</th>
	<th>Action</th>
  </tr>
	@foreach($order as $pesanan)
	<tr>

<form action="/konfirmasipembayaran/{{$pesanan->id}}" method="POST" enctype="multipart/form-data">
@csrf
@method('PUT')
<td><span>{{ $pesanan->user->name }}</span></td>
<td><span>{{ $pesanan->date }}</span></td>
@if ($pesanan->status == 1)
<td><span class="p-2 badge badge-danger w-100" name="namaproduk" >{{ $pesanan->status }} | Menunggu Konfirmasi Pembayaran</span></td>
@else
<td><span class="p-2 badge badge-primary w-100" name="namaproduk" >{{ $pesanan->status }} | Pembayaran Terkonfirmasi</span></td>
@endif
<td><span>{{ $pesanan->total_price }}</span></td>



<td>
<input  class="data inputbutton btn-lg	" type="submit" style="background:hsl(150,100%,30%);" value="Konfirmasi Pembayaran Pesanan">
</form>
</td>

</tr>
@endforeach
</table>
<br>
<p>* Status 1 : Sudah pesan dan sedang menunggu konfirmasi pembayaran.</p>
<p>* Status 2 : Sudah membayar dan sedang menunggu proses pengiriman.</p>





</div>
@endsection
