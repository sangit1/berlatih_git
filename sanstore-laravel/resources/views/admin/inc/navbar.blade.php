 <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
     navbar-scroll="true">
     <div class="container-fluid py-1 px-3">
         {{-- <nav aria-label="breadcrumb">
             <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                 <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
                 <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Dashboard</li>
             </ol>
            </nav> --}}
         <h5 class="font-weight-bolder mb-2">Dashboard</h5>
         <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
             <ul class="navbar-nav ms-auto">
                 <li class="nav-item d-flex align-items-center">
                     <a href="" class="nav-link text-body font-weight-bold px-0 mx-3">
                         <i class="fa fa-user me-sm-1" aria-hidden="true"></i>
                         <span class="d-sm-inline d-none">{{ auth()->user()->name }}</span>
                     </a>
                 </li>
                 <li class="nav-item d-flex align-items-center">
                     <a class="nav-link text-body font-weight-bold px-0" href="{{ route('logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                         <i class="fa fa-lock me-sm-1" aria-hidden="true"></i>
                         <span class="d-sm-inline d-none">logout</span>
                     </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                         @csrf
                     </form>
                 </li>
             </ul>
         </div>
     </div>
 </nav>
