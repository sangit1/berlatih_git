<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
        <img src="{{ asset('adminlte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{Auth::user()->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav  nav-sidebar" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview ">
                    <a href="/admin" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                <li class="nav-item has-treeview">
                    <a href="{{ url('dataproduct') }}" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Update Produk
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ url('category') }}" class="nav-link">
                        <i class="nav-icon fas fa-stream"></i>
                        <p>
                            Update Kategori
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ url('pesanan') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            Update Status Pesanan
                        </p>
                    </a>
                </li>
        <li class="nav-item has-treeview">
                    <a href="{{ url('') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Kembali ke Home
                        </p>
                    </a>
                </li>
        <li class="nav-item has-treeview">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="nav-icon fas fa-pen-square"></i>
                        <p>
                            Kustomisasi Template
                        </p>
                    </a>
                </li>

==

<a  class="btn btn-outline-primary w-100" href="{{ route('logout') }}" onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
                <i class="fa fa-lock me-sm-1" aria-hidden="true"></i>
                <span class="d-sm-inline d-none">Logout</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none" >
                @csrf
            </form>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
